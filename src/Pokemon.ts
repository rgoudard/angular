export class Pokemon {
    name: string;
    speed: number;
    damage: number;
    life: number;



    constructor(name: string, speed: number, attack: number, life: number) {
        this.name = name;
        this.speed = speed;
        this.damage = attack;
        this.life = life;
    }

    fight(pkm1: Pokemon, pkm2: Pokemon): Pokemon{
        while (pkm1.life > 0 && pkm2.life > 0){
            if (this.attackFirst(pkm1,pkm2)){
                this.attack(pkm1,pkm2);
            }else{
                this.attack(pkm2,pkm1);
            }
        }
        if (pkm1.life <= 0){
            console.log(pkm2.name + " has win, congratulations !!")
            return pkm2;
        }
        if (pkm2.life <= 0){
            console.log(pkm1.name + " has win, congratulations !!")
            return pkm1;
        }
        return pkm2;
    }

    attackFirst(pkm1: Pokemon, pkm2: Pokemon): boolean{
        if(pkm1.speed > pkm2.speed){
            console.log(pkm1.name + " attack first !");
            return true;
        }else{
            console.log(pkm2.name + " attack first !");
            return false;
        }
    }

    attack(pkmAttacker: Pokemon, pkmDefender: Pokemon){
        console.log(pkmAttacker.name + " set " + pkmAttacker.damage + " damage to " + pkmDefender.name);
        this.looseLife(pkmAttacker, pkmDefender);
        if(pkmDefender.life > 0){
            this.attack(pkmDefender,pkmAttacker)
        }else{
            console.log(pkmDefender.name + " is already dead")
        }
    }

    looseLife(pkmAttacker : Pokemon, pkmDefender: Pokemon){
        pkmDefender.life = pkmDefender.life - pkmAttacker.damage;
    }
}