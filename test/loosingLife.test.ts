import {Pokemon} from "../src/Pokemon";

describe("loosingLife",()=>{
    it("loosing 5 hit points", ()=>{
        let pkm1 = new Pokemon("Pikachu", 3 , 5, 20);
        let pkm2 = new Pokemon("Tortank",1, 8 , 40);
        pkm1.looseLife(pkm1,pkm2);
        expect(pkm2.life).toEqual(35);
    })
});