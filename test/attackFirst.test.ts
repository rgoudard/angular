import {Pokemon} from "../src/Pokemon";

describe("firstAttacker",()=>{
    it("true", ()=>{
        let pkm1 = new Pokemon("Pikachu", 3 , 5, 20);
        let pkm2 = new Pokemon("Tortank",1, 8 , 40);
        expect(pkm1.attackFirst(pkm1,pkm2)).toEqual(true);
    });
    it("false", ()=>{
        let pkm2 = new Pokemon("Pikachu", 3 , 5, 20);
        let pkm1 = new Pokemon("Tortank",1, 8 , 40);
        expect(pkm1.attackFirst(pkm1,pkm2)).toEqual(false);
    })
});