import {Pokemon} from "../src/Pokemon";

describe("PokemonAttribute",()=>{
    it("name", ()=>{
        let pkm1 = new Pokemon("Bonjour", 3 , 5, 20);
        expect(pkm1.name).toEqual("Bonjour");
    });
    it("damage", ()=>{
        let pkm1 = new Pokemon("Bonjour", 3 , 5, 20);
        expect(pkm1.damage).toEqual(5);
    });
    it("speed", ()=>{
        let pkm1 = new Pokemon("Bonjour", 3 , 5, 20);
        expect(pkm1.speed).toEqual(3);
    });
    it("life", ()=>{
        let pkm1 = new Pokemon("Bonjour", 3 , 5, 20);
        expect(pkm1.life).toEqual(20);
    })
});