import {Pokemon} from "../src/Pokemon";

describe("fight",()=>{
    it("Pkm1 is the winner", ()=>{
        let pkm1 = new Pokemon("Pikachu", 3 , 5, 20);
        let pkm2 = new Pokemon("Tortank",1, 8 , 15);
        expect(pkm1.fight(pkm1,pkm2)).toEqual(pkm1);
    });
    it("Pkm2 is the winner", ()=>{
        let pkm1 = new Pokemon("Pikachu", 3 , 5, 16);
        let pkm2 = new Pokemon("Tortank",1, 8 , 15);
        expect(pkm1.fight(pkm1,pkm2)).toEqual(pkm2);
    })
});